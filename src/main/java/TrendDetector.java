import java.util.Timer;
import java.util.TimerTask;

public class TrendDetector {
    public static final int BUCKET_INTERVAL = 10; // sec
    public static final int EVENT_COUNT_THRESHOLD = 20;
    public static final double CONFIDENCE_INTERVAL_CONSTANT = 2.58; // for 0.99 confidence level
    public static final double UNLIKELINESS = 1.0;

    private Timer updateBucketTimer;

    private TimeBucket previousBucket = null;
    private TimeBucket currentBucket = null;

    public TrendDetector() {
        final TrendDetector detector = this;
        updateBucketTimer = new Timer(true);
        updateBucketTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                detector.updateBucket();
            }
        }, 0, BUCKET_INTERVAL * 1000);
    }

    private void updateBucket() {
        previousBucket = currentBucket;
        currentBucket = new TimeBucket();

        System.out.println("bucket 1: " + (previousBucket == null ? 0 : previousBucket.getCount("1")));
        System.out.println("bucket 2: " + (previousBucket == null ? 0 : previousBucket.getCount("2")));
        System.out.println("bucket 3: " + (previousBucket == null ? 0 : previousBucket.getCount("3")));
        System.out.println("bucket updated");
    }

    public void handleEvent(String label) {
        long currentCount = currentBucket.insertAndGetCount(label);
        long previousCount = previousBucket == null ? 0 : previousBucket.getCount(label);

        if (previousCount < EVENT_COUNT_THRESHOLD || currentCount < EVENT_COUNT_THRESHOLD) {
            return;
        }

        long currentBucketLifeSpan = currentBucket.lifeSpan();
        double mean = previousCount * currentBucketLifeSpan / (BUCKET_INTERVAL * 1000.0);

        if (detectTrend(mean, currentCount)) {
            System.out.println(label + " is trending!");
        }
    }

    private boolean detectTrend(double mean, double observation) {
        return mean + UNLIKELINESS * CONFIDENCE_INTERVAL_CONSTANT * Math.sqrt(mean) < observation;
    }

    public void stop() {
        updateBucketTimer.cancel();
    }
}
