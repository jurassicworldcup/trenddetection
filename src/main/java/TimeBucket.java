import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;

public class TimeBucket {
    long timestamp;
    HashMap<String, AtomicLong> bucket;

    public TimeBucket() {
        timestamp = System.currentTimeMillis();
        bucket = new HashMap<String, AtomicLong>();
    }

    public long insertAndGetCount(String label) {
        bucket.putIfAbsent(label, new AtomicLong(0));
        return bucket.get(label).incrementAndGet();
    }

    public long getCount(String label) {
        AtomicLong count = bucket.get(label);
        if (count == null) {
            return 0;
        } else {
            return count.get();
        }
    }

    public long lifeSpan() {
        return System.currentTimeMillis() - timestamp;
    }
}
