import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class TrendDetectionApp {
    public static void main(String args[]) throws IOException {
        TrendDetector detector = new TrendDetector();

        ServerSocket serverSocket = new ServerSocket(1234);
        Socket clientSocket = serverSocket.accept();

        BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        String input;

        while ((input = in.readLine()) != null) {
            detector.handleEvent(input);
        }

        in.close();
        clientSocket.close();
        serverSocket.close();

        detector.stop();
    }
}
