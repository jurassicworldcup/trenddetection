'use strict';

const Socket = require('net').Socket;

let client = new Socket();

client.connect(1234, '127.0.0.1', () => {
  var stdin = process.stdin;

  stdin.setRawMode(true);
  stdin.resume();
  stdin.setEncoding('utf-8');

  stdin.on('data', (key) => {
    if (key === '\u0003') {
      process.exit();
    }
    process.stdout.write(key);
    client.write(key + '\n');
  });
});
